package org.sbx.Pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Status {
    @JsonProperty("statusCode")
    private String statusCode;

    @JsonProperty("description")
    private String description;

}
