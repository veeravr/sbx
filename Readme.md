# E2E Rest API - Assessment test pack

This repository contains the API framework build in Cucumber - RestAssured

The Mock API's in the Test pack should be called from the sprint application for test harness, which is a Jar file
/riretail-0.0.1-SNAPSHOT
 - Attached on the repo under SpringAppJarFile
 - before you run the test pack run the jar file from the cmd prompt
 - C://desktop/java -jar riretail-0.0.1-SNAPSHOT.jar

----
Test Approach : 
--
The API's used here are designed to use

GET , POST calls

Test runner is provided to run for any tags we want to  @smoke , @Regression or @E2E

The Endpoints can be mapped from the properties file to manage them when we deploy it on the real AUT.

All the test results and Test reports are genereted on the Target folder.

The API's endpoints used in the current framework are designed and mocked with the positive and negitive cases.

Run the Scenario's from the TestRunner class or from the Feature file.

The API's I've used for the test harness are build on smartmock to template the 200, 400, 404  -- BAD REQUEST,UNAUTHORIZED 

considered additional testing aspects - Oauth2.0 Token , Invalid Token and Expired Token Scenarios

Improvements can be made to the framework with the reporting , however the usual way of cucumber reporting is mostly handled in Jenkins pipeline to send an email when the test completed.


-------
Findings and Observations : 
--
- Spring boot application has enormous ways to handle the beans and the Request calls , it all comes to the ability which dependency we are choosing for example: we can use wiremock server to mock or we can use any other mocking we are familiar with, for this test I've used smartMock.
  

- Bundling the applications sometimes throws exceptions with the @RequestMappings , @ComponentScan where the packages hasn't been scanned while the jar is running.
  

- the port might be in use and when we run the spring application the tomcat server doesn't launch. we can manage the server port from the app properties or from the @SpringBootTest annotation to pass if it should be random or defined.
  
 


