Feature: Customer Overview E2E Test Coverage

  @E2E @CustomerJourney
  Scenario: Verify the API for search customer by valid ID
    Given the valid token is provided
    When the "GET" request is triggered to "/api/v1/customer/{id}" with "E31256"
    Then I verify the response code is "200"
    And I verify the response body should contain the field "address.line2" as "Emarald Palace St"
    And I verify the status code is "ICO9001"
    And I verify the response body should contain the field "referenceId" as "GoldWarriorA13"

  @E2E
  Scenario: Verify Invalid Token response
    Given  I enter the invalid token
    When the "GET" request is triggered to "/api/v1/customer/{id}" with "E31256"
    Then I verify the response code is "400"
    And I verify the response body should contain the field "message" as "Invalid Token"

  @E2E
  Scenario:Verify customer by valid email
    Given the valid token is provided
    When the "GET" request is triggered to query "/api/v1/customer?" with "john.doe@dreq.com"
    Then I verify the response code is "200"

  @E2E
  Scenario:Verify No data exist when by valid email
    Given the valid token is provided
    When the "GET" request is triggered to query "/api/v1/customer?" with "troman@dreq.com"
    Then I verify the response code is "404"
    And I verify the response body should contain the field "errorMessage" as "No Data found"
    And I verify the response body should contain the field "statusCode" as "IDO9002"

  @E2E
  Scenario:Verify the Api call is unauthorized
    Given I enter the invalid token
    When the "GET" request is triggered to query "/api/v1/customer?" with "john.doe@dreq.com"
    Then I verify the response code is "401"
    And I verify the response body should contain the field "message" as "Unauthorized"

    @E2E
  Scenario: create customer with valid token
    Given the valid token is provided
    When the POST request is triggered to create "/api/v1/customer" with "201"
    Then I verify the response code is "201"

  @E2E
  Scenario:Verify Expired token when a valid email sent
    Given the expired token is provided
    When the "GET" request is triggered to query "/api/v1/customer?" with "john.doe@dreq.com"
    Then I verify the response code is "400"

    @E2E
    Scenario: Verify the OauthToken is Validated
      Given I call the Oauth Token API with the SecretCredentials
       Then I verify the TokenType is matched to "Bearer"
