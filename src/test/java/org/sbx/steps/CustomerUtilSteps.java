package org.sbx.steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;
import org.sbx.token.TokenTemplate;

import static io.restassured.RestAssured.given;
public class CustomerUtilSteps {
    private Response response;
    private TokenTemplate tokenArray;
    private String accessToken;
    private final static String validToken = "Bearer PkBjmrSVfbLWZvRsUDMmh9QPjrymYeMyIlZYfMtTAYMF263w4XZsvOIAyWAofy6b";
    private final static String inValidToken = "Bearer 1PkBjmrSVfbLWZvRsUDMmh9QPjrymYeMyIlZYfMtTAYMF263w4XZsvOIAyWAofy6b";
    private final static String expiredToken = "Bearer ExpiredPkBjmrSVfbLWZvRsUDMmh9QPjrymYeMyIlZYfMtTAYMF263w4XZsvOIAyWAofy6b";


    @Before
    public void setDefaults() {
        RestAssured.baseURI = "http://localhost:8080";
        accessToken = validToken;


    }

    @Given("the valid token is provided")
    public void theValidTokenIsProvided() {
        accessToken = validToken;
    }
    @Given("the expired token is provided")
    public void theExpiredTokenIsProvided() {
        accessToken = expiredToken;
    }
    @When("the {string} request is triggered to {string} with {string}")
    public void theRequestIsTriggeredToWith(String verb, String endPoint, String id) {
        try {
            response =
                    given().contentType("application/json")
                            .accept("application/json")
                            .header("Authorization", accessToken)
                            .when().get(endPoint, id);
//                        .then()
//                        .statusCode(200).extract().response();
            response.then().log().all();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @When("the {string} request is triggered to query {string} with {string}")
    public void the_request_is_triggered_to_query_with(String verb, String endPoint, String id) {
        try {
            response =
                    given().contentType("application/json")
                            .accept("application/json")
                            .header("Authorization", accessToken)
                            .when().get(endPoint+"email="+id);
//                        .then()
//                        .statusCode(200).extract().response();
            response.then().log().all();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @Then("I verify the response code is {string}")
    public void iVerifyTheResponseCodeIs(String responseCode) {
        response.then().statusCode(Integer.valueOf(responseCode));
    }

    @And("I verify the response body should contain the field {string} as {string}")
    public void iVerifyTheResponseBodyShouldContainTheFieldAs(String key, String value) {
        response.jsonPath().get(key).equals(value);

//        response.then().body(key, equalTo(value));
    }

    @And("I verify the status code is {string}")
    public void iVerifyTheStatusCodeIs(String statusCode) {
        response.jsonPath().get("status.statusCode").equals(statusCode);
    }

    @Given("I enter the invalid token")
    public void i_enter_the_invalid_token() {
        accessToken = inValidToken;
    }

    @When("the POST request is triggered to create {string} with {string}")
    public void theRequestIsTriggeredToCreateWith(String endPoint, String id) {
        try {
            response =
                    given().contentType("application/json")
                            .accept("application/json")
                            .header("Authorization", accessToken)
                            .when().post(endPoint);

//            response.then().log().all();
            response.then().statusCode(Integer.valueOf(id));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Given("I call the Oauth Token API with the SecretCredentials")
    public void iCallTheOauthTokenAPIWithTheSecretCredentials() {

        try {
            response =
                    given().contentType("application/json")
                            .accept("application/json")
                            .header("Authorization", accessToken)
                            .when()
                            .body("{\n" +
                                    "    \"grant_type\":\"client_credentials\",\n" +
                                    "    \"client_secret\":\"1cb96c54-04e1-4b61-92ed-d2c5a6e127f3\",\n" +
                                    "    \"client_id\":\"gamer\"\n" +
                                    "}")
                            .get("https://neat-turkey-49.app.smartmock.io/oauth2/token")
                        ;

//            response.then().log().all();
            response.then().statusCode(200);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Then("I verify the TokenType is matched to {string}")
    public void iVerifyTheTokenTypeIsMatchedTo(String arg0) {
        Assert.assertTrue(response.jsonPath().get("token_type").equals("Bearer"));
    }
}
