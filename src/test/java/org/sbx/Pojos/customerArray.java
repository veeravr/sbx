package org.sbx.Pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
        import lombok.Data;

@Data
public class customerArray {
    @JsonProperty("id")
    private String id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("status")
    private Status status;
}
